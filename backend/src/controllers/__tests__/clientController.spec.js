import { get, post } from '../clientController';

jest.mock('get');

const mockClientsList = [
  {
    _id: 1,
    name: 'John Smith',
    email: 'john@smith.com',
    company: 'EValue',
    createdDate: '010221',
  },
  {
    _id: 2,
    name: 'Lisa Smith',
    email: 'lisa@smith.com',
    company: 'EValue',
    createdDate: '081121',
  },
];

test('Returns list of clients', async () => {
  get.mockResolvedValue({
    data: [
      {
        _id: 1,
        name: 'John Smith',
        email: 'john@smith.com',
        company: 'EValue',
        createdDate: '010221',
      },
      {
        _id: 2,
        name: 'Lisa Smith',
        email: 'lisa@smith.com',
        company: 'EValue',
        createdDate: '081121',
      },
    ],
  });

  const clientsList = await get();
  expect(clientsList).toEqual(mockClientsList);
});

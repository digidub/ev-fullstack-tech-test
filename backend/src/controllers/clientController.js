import Client from '../models/client';
import 'regenerator-runtime/runtime';

const searchQuery = async (query) => {
  const clientSearch = await Client.find({
    name: query,
  }).collation({ strength: 1, locale: 'en' });
  if (!clientSearch) return null;
  return clientSearch;
};

export const get = async (req, res) => {
  console.log(req);
  let results;
  try {
    if (req.query.name) {
      results = await searchQuery(req.query.name);
    } else results = await Client.find({}).sort({ name: 1 });
    if (!results.length) return res.json(null);
    return res.json(results);
  } catch (err) {
    res.status(400).json(err);
  }
};

export const post = async (req, res) => {
  try {
    const newClient = new Client({
      name: req.body.name,
      email: req.body.email,
      company: req.body.company,
    });
    await newClient.save();
    res.json(newClient);
  } catch (err) {
    res.status(400).json(err);
  }
};

import clients from '../clients';
import request from 'supertest';
import express from 'express';
import 'regenerator-runtime/runtime';

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use('/clients', clients);

test('clients route works', (done) => {
  request(app)
    .get('/clients')
    .expect('Content-Type', '/json/')
    .expect({ name: 'Alice Adams' })
    .expect(200, done);
});

test('posting client route works', (done) => {
  request(app)
    .post('/clients')
    .type('form')
    .send({ name: 'John Smith', email: 'john@smith.com', company: 'EValue' })
    .then(() => {
      request(app)
        .get('/clients')
        .expect(
          { name: 'John Smith', email: 'john@smith.com', company: 'EValue' },
          done
        );
    });
});

import mongoose from 'mongoose';
const { Schema } = mongoose;

const ClientSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  createdDate: { type: Date, required: true, default: Date.now },
  company: { type: String, required: true },
});

const Client = mongoose.model('Client', ClientSchema);

export default Client;

import React from 'react';
import NewClient from './components/NewClient';
import SearchClient from './components/SearchClient';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import styled from 'styled-components';

const App = () => {
  return (
    <AppWrapper>
      <BrowserRouter>
        <Navbar />
        <BodyWrapper>
          <Routes>
            <Route exact path='/' element={<SearchClient />} />
            <Route exact path='/new' element={<NewClient />} />
          </Routes>
        </BodyWrapper>
      </BrowserRouter>
    </AppWrapper>
  );
};

export default App;

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const BodyWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

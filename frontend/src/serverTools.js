const ServerTools = (() => {
  const handleErrors = (res) => {
    if (!res.ok) throw res;
    return res.json();
  };

  const getRequestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const postRequestOptions = (object) => {
    return {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(object),
    };
  };

  const get = (url) => {
    return fetch(url, getRequestOptions)
      .then((res) => res.json())
      .then((data) => data)
      .catch((err) => {
        console.log(err);
        return err;
      });
  };

  const post = (url, newObject) => {
    return fetch(url, postRequestOptions(newObject))
      .then(handleErrors)
      .then((data) => data)
      .catch((err) => {
        console.log(err);
        return err;
      });
  };

  return {
    get,
    post,
  };
})();

export default ServerTools;

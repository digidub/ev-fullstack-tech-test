import ServerTools from '../serverTools';
import 'regenerator-runtime/runtime';

const mockClient = {
  _id: 1,
  name: 'John Smith',
  email: 'john@smith.com',
  company: 'EValue',
  createdDate: '010221',
};

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(mockClient),
  })
);

beforeEach(() => {
  fetch.mockClear();
});

test('fetches client data', async () => {
  const client = await ServerTools.get('http://localhost:3001/clients');
  expect(client).toEqual(mockClient);
});

test('posts client data', async () => {
  const postedClient = await ServerTools.post(
    'http://localhost:3001/clients',
    mockClient
  );
  expect(postedClient).toEqual(mockClient);
});

import React, { useState } from 'react';
import styled from 'styled-components';
import ClientList from './ClientList';

const SearchClient = () => {
  const [searchText, setSearchText] = useState('');
  const [query, setQuery] = useState(null);

  const handleSearchInput = (e) => {
    setSearchText(e.target.value);
  };

  const handleSearchButton = (e) => {
    e.preventDefault();
    const encodedSearchText = encodeURIComponent(searchText);
    const url = `http://localhost:3001/clients?name=${encodedSearchText}`;
    setQuery(url);
  };

  const handleClearButton = (e) => {
    e.preventDefault();
    setQuery(null);
  };

  return (
    <div>
      <form>
        <SearchBox
          type='text'
          value={searchText}
          onChange={handleSearchInput}
        />
        <input type='submit' value='search' onClick={handleSearchButton} />
        <input type='submit' value='clear' onClick={handleClearButton} />
      </form>
      <ClientList query={query} />
    </div>
  );
};

export default SearchClient;

const SearchBox = styled.input`
  border: 1px solid #ccc;
  font-size: 15px;
  width: 70%;
  text-decoration: none;
  padding: 5px 0 5px 5px;
`;

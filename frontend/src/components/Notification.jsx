import React from 'react';
import styled from 'styled-components';

const Notification = ({ message }) => {
  return <Success>{message}</Success>;
};

export default Notification;

const Success = styled.p`
  color: green;
`;

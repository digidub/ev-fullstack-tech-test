import React, { useState } from 'react';
import ServerTools from '../serverTools';
import Notification from './Notification';

const NewClient = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [company, setCompany] = useState('');
  const [notification, setNotification] = useState(null);

  const handleNameInput = (e) => {
    setName(e.target.value);
  };

  const handleEmailInput = (e) => {
    setEmail(e.target.value);
  };

  const handleCompanyInput = (e) => {
    setCompany(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newClient = {
      name,
      email,
      company,
    };
    const url = 'http://localhost:3001/clients';
    const response = await ServerTools.post(url, newClient);
    console.log(response);
    setNotification(`successfully created client ${response.name}`);
  };

  return (
    <div>
      {notification ? <Notification message={notification} /> : null}
      <form>
        <label htmlFor='name'>Name:</label>
        <input
          type='text'
          name='name'
          value={name}
          onChange={handleNameInput}
        />
        <label htmlFor='email'>Email:</label>
        <input
          type='text'
          name='email'
          value={email}
          onChange={handleEmailInput}
        />
        <label htmlFor='company'>Company:</label>
        <input
          type='text'
          name='company'
          value={company}
          onChange={handleCompanyInput}
        />
        <input type='submit' onClick={handleSubmit} value='Add Client' />
      </form>
    </div>
  );
};

export default NewClient;

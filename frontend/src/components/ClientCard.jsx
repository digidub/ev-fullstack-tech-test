import React from 'react';
import styled from 'styled-components';
import { format } from 'date-fns';

const ClientCard = ({ name, email, createdDate, company }) => {
  const date = format(new Date(createdDate), 'dd MMM yyyy');

  return (
    <Card>
      <CardHeader>
        <p>{name}</p>
        <p>{company}</p>
      </CardHeader>
      <p>{email}</p>
      <p>Created: {date}</p>
    </Card>
  );
};

export default ClientCard;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 400px;
  height: 150px;
  margin: 5px;
  box-shadow: 0 1px 2px rgb(0 0 0 / 20%);
`;

const CardHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

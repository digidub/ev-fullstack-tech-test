import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const Navbar = () => {
  return (
    <NavBar>
      <NavLinks>
        <li>
          <StyledLink to='/'>Client List</StyledLink>
        </li>
        <li>
          <StyledLink to='/new'>New Client</StyledLink>
        </li>
      </NavLinks>
    </NavBar>
  );
};

export default Navbar;

const NavBar = styled.div`
  background-color: #fff;
  top: 0;
  left: 0;
  width: 100%;
  height: 50px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const NavLinks = styled.ul`
  width: 800px;
  max-width: 800px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  max-width: 600px;
  margin: 0 auto;
  list-style-type: none;
`;

const StyledLink = styled(NavLink)`
  color: black;
  text-decoration: none;
  font-weight: 600;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
  &:hover {
    color: #afe1af;
    transition: all 0.1s;
  }
`;

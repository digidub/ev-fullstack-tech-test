import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ServerTools from '../serverTools';
import ClientCard from './ClientCard';

const ClientList = ({ query }) => {
  const [clients, setClients] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [noResults, setNoResults] = useState(null);

  useEffect(() => {
    setNoResults(false);
    const url = query ? query : 'http://localhost:3001/clients';
    console.log(url);
    ServerTools.get(url)
      .then((data) => {
        if (data === null) setNoResults(true);
        setClients(data);
        setLoading(false);
      })
      .catch((err) => setError(err));
  }, [query]);

  const listClients = () =>
    clients.map((client) => {
      return (
        <ClientCard
          key={client._id}
          name={client.name}
          email={client.email}
          createdDate={client.createdDate}
          company={client.company}
        />
      );
    });

  return (
    <ClientListContainer>
      {loading && <p>loading data</p>}
      {clients && <div>{listClients()}</div>}
      {error ? <p>Error</p> : null}
      {noResults ? <p>No results match your search</p> : null}
    </ClientListContainer>
  );
};

export default ClientList;

const ClientListContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
